FROM --platform=amd64 python:3.11

RUN python -m pip install requests
ADD hello.py /analysis/hello.py
WORKDIR /analysis